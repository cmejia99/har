﻿###Función cuya tarea es renombrar los archivos Json:

function rename_file($Raiz, $Directorio, $path) 
{
	
	$Ruta = $Raiz + $Directorio + '/*'
	$Iterador = 0
	$files = Get-ChildItem $Ruta
	foreach ($f in $files)
	{
		$i = $i + 1
		$outfile = $f.Name 
		$Original = $path + $Raiz + $Directorio + '/' + $outfile
		
		$New_name = $Directorio+'_'+$i+'.json'
		$Nuevo    = $path + $Raiz + $Directorio + '/' + $New_name
				
		echo $outfile' Nuevo_nombre: '$New_name
		#echo $Original
		#echo $Nuevo
		Rename-Item $Original -NewName $Nuevo
	}
}

$lc = Get-Location
$Ruta = $lc.Path+'/'
echo 'Directorio '$Ruta

##Llamado de la funcion de ajuste de nombre: (Cambiar el segundo parámetro por el nombre de etiqueta).

#rename_file 'DataSet/' 'Sentarse' $Ruta
#rename_file 'DataSet/' 'Girando' $Ruta
#rename_file 'DataSet/' 'Escaleras' $Ruta
#rename_file 'DataSet/' 'Caminar' $Ruta
#rename_file 'DataSet/' 'Reposar' $Ruta 
rename_file 'DataSet/' 'Trotar' $Ruta

pause