# **Human Activity Recognition (HAR) unidimensionales con redes convolucionales y redes recurrentes** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Planteamiento de la solución** ##
Se tiene como propuesta de solución a nuestro proyecto utilizar las siguientes arquitecturas:

*   **Red Convolucional Clásica 1D**
*   **Red Recurrente LSTM**
*   **Red Recurrente GRU**

Las estructuras de cada modelo se describirán a continuación:

*   **Red Convolucional Clásica 1D** Se realizó entrenamiento con la arquitectura que se presentará a continuación con un total de **50 épocas** y tamaña de **batch 10** para un total de 120 batch por época.
    ![img1](Resultados/1D_1.png)   

     ![img1](Resultados/1D_2.png)      

*   **Red Recurrente LSTM:** Se realizó entrenamiento con la arquitectura que se presentará a continuación con un total de **50 épocas** y tamaña de **batch 10** para un total de 120 batch por época.

     ![img1](Resultados/lstm_1.png)   

     ![img1](Resultados/lstm_2.png)    

*   **Red Recurrente GRU** Se realizó entrenamiento con la arquitectura que se presentará a continuación con un total de **50 épocas** y tamaña de **batch 10** para un total de 120 batch por época.

     ![img1](Resultados/gru_1.png)   

     ![img1](Resultados/gru_2.png)  

## **Estructuración del repositorio** ##
El repositorio se estructura de la siguiente manera:

* **MiniProyecto_1.ipynb:** Archivo [Google Colab](https://colab.research.google.com/) con la recopilación de los pasos llevados a cabo para la implementación, entrenamiento y generación de resultados de nuestros modelos.

## **Resultados** ##
A continuación se presenta los resultados obtenidos en el entrenamiento y validación de los modelos propuestas como solución a nuestro problema, en validación se utilizó la métrica Accuracy y la matriz de confusión.

*   **Red Convolucional Clásica 1D** Los resultados obtenidos con esta arquitectura son:

    1.  Validación loss y accuracy en fase de entrenamiento:

        ![img1](Resultados/mod_1D.png)  

    2.  Validación matriz de confusión sobre el data set de validación:

        ![img1](Resultados/conf_1D.png)  

*   **Red Recurrente LSTM:** Los resultados obtenidos con esta arquitectura son:

    1.  Validación loss y accuracy en fase de entrenamiento:

        ![img1](Resultados/mod_lstm.png)  

    2.  Validación matriz de confusión sobre el data set de validación:

        ![img1](Resultados/conf_lstm.png)   

*   **Red Recurrente GRU:** Los resultados obtenidos con esta arquitectura son:

    1.  Validación loss y accuracy en fase de entrenamiento:

        ![img1](Resultados/mod_gru.png)  

    2.  Validación matriz de confusión sobre el data set de validación:

        ![img1](Resultados/conf_gru.png)   
        

## **Conclusiones** ##
Al validar las distintas arquitecturas de modelos entrenados no fue posible mejorar los resultados en el data set de validación, pero si se puedo encontrar buenos resultados en el dataset de entrenamiento. Concluimos que el modelo que presento un mejor equilibrio de resultados es el GRU V2 entre el data set de entrenamiento y validación. Adicionalmente, es bueno resultar que este modelo no presenta dropout y es el que tiene menos parámetros en su estructura.

## **Referencias:** ##
* https://campus.uaovirtual.edu.co/pluginfile.php/422365/mod_resource/content/1/Presentacion_3_DS_DL_2021.pdf
* https://campus.uaovirtual.edu.co/mod/resource/view.php?id=139777
* https://campus.uaovirtual.edu.co/mod/resource/view.php?id=175213
* https://studio.edgeimpulse.com/login
