# **Human Activity Recognition (HAR) unidimensionales con redes convolucionales y redes recurrentes** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Contenido** ##
En este repositorio se podrá encontrar la documentación, fuentes y resultados llevados a cabo en la construcción del mini proyecto HAR de la materia de modelos secuenciales, especialización de inteligencia artificial - Universidad Autónoma de Occidente.

## **Descripción del problema a solucionar** ##
Con este proyecto se tiene como propósito generar un modelo de inteligencia artificial, el cual nos permite clasificar señales secuenciales de un dispositivo móvil (celular) en las siguientes clases:

![img](img/signal.png)  

*   **Escaleras**
*   **Girando**
*   **Sentarse**
*   **Trotar**
*   **Caminar**
*   **Reposo**

## **Estructuración del repositorio** ##
El repositorio se estructura de la siguiente manera:

* **DataSet:** Directorio donde se registró todos los archivos Json de las señalas detectadas por medio de un dispositivo celular a través de la herramienta [EDGE IMPULSE](https://studio.edgeimpulse.com/)

    Los archivos de muestras se tomaron en intervalos de 10 segundos por un total de 20 minutos por clase. La aplicación [EDGE IMPULSE](https://studio.edgeimpulse.com/) genera archivos con información de 10 segundos por lo que se podrá encontrar 120 archivos Json de los resultados.

    ![img1](img/samples.png)

    ![img2](img/Trotar.png)

    Las clases objetivo del proyecto serán las siguientes:

    *   **Escaleras:** Valores tomados realizando el proceso de subir y bajar escaleras en distintas velocidades.
    *   **Girando:** Valores tomados realizando giros sobre su propio eje hacia ambos sentidos (Izquierda a derecha y Derecha a izquierda).
    *   **Sentarse:** Valores tomados realizando el proceso de sentarse y pararse.
    *   **Trotar:** Valores tomados realizando trote de manera constante.
    *   **Caminar:** Valores tomados realizando caminata constante a un paso lento.
    *   **Reposo:** Valores tomados en estado reposo.

   Se distribuyo el data set generado en dos grupos, uno para entrenamiento y otro para validación, los cuales presentarán 100 archivos para entrenamiento por clase (aproximadamente **374.400**) y 20 archivos para validación por clase (aproximadamente **74.880**). La distribución de los directorios se podrá visualizar a continuación:
    ![img3](img/Repositorio.png)    

* **Modelos:** Directorio donde se registró todos los fuentes, modelos y resultados generados en la construcción del proyecto.

## **Referencias:** ##
* https://campus.uaovirtual.edu.co/pluginfile.php/422365/mod_resource/content/1/Presentacion_3_DS_DL_2021.pdf
* https://campus.uaovirtual.edu.co/mod/resource/view.php?id=139777
* https://campus.uaovirtual.edu.co/mod/resource/view.php?id=175213
* https://studio.edgeimpulse.com/login